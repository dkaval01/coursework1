/**
 * @author Domantas Kavaliauskas
 * created on 11/11/2015
 */
class Point {
    private int x = 0;
    private int y = 0;

    /**
     * default constructor
     */
    public Point() {
    }

    /**
     * A constructor of Point
     * @param x coordinate on the x axis
     * @param y coordinate on the y axis
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns a string representation of object Point in the format (x,y), where x and y are an instance's coordinates.
     * @return a string representation of Point.
     */
    @Override public String toString(){
        return ("(" + x + "," + y + ")");
    }

    /**
     * @return x coordinate of an instance
     */
    public int getX() {
        return this.x;
    }

    /**
     * @return y coordinate of an instance
     */
    public int getY() {
        return this.y;
    }

    /**
     * Returns the smallest Rectangle that contains all points in points.
     * @param points an array of Point class;
     * @return Rectangle if some none null points entered, and null otherwise;
     */
    public static Rectangle boundingBox(Point[] points) {
        if(points.length == 0) {
            return null;
        }

        int smallX = points[0].getX();
        int smallY = points[0].getY();
        int bigX = points[0].getX();
        int bigY = points[0].getY();

        for (Point point : points) {
            if(point == null) {
                return null;
            }
            if (smallX > point.getX()) {
                smallX = point.getX();
            }
            if (smallY > point.getY()) {
                smallY = point.getY();
            }
            if (bigX < point.getX()) {
                bigX = point.getX();
            }
            if (bigY < point.getY()) {
                bigY = point.getY();
            }
        }

        Point smallPoint = new Point(smallX, smallY);
        Point bigPoint =  new Point(bigX, bigY);
        return (new Rectangle(smallPoint, bigPoint));
    }
}

