import static java.lang.Math.abs;
/**
 * @author Domantas Kavaliauskas
 *         created on 11/11/2015
 */
public class Rectangle {
    private Point lowerLeft = new Point();
    private Point lowerRight = new Point();
    private Point upperLeft = new Point();
    private Point upperRight = new Point();

    /**
     * Constructs a Rectangle from two Point's, by comparing x and y coordinates and creates four new Points
     * corresponding to Rectangle corners.
     * @param point1 a building Point which lies in the opposite corner of point 2;
     * @param point2 a Point which lies in the opposite corner of point 1;
     */
    public Rectangle(Point point1, Point point2) {
        int smallX;
        int smallY;
        int bigX;
        int bigY;
        if(point1.getX()<point2.getX()){
            smallX = point1.getX();
            bigX = point2.getX();
        }
        else {
            smallX = point2.getX();
            bigX = point1.getX();
        }
        if(point1.getY()<point2.getY()){
            smallY = point1.getY();
            bigY = point2.getY();
        }
        else {
            smallY = point2.getY();
            bigY = point1.getY();
        }

        this.lowerLeft = new Point(smallX, smallY);
        this.lowerRight = new Point(bigX, smallY);
        this.upperLeft = new Point(smallX, bigY);
        this.upperRight = new Point(bigX, bigY);
    }

    /**
     * Gets lowerLeft Point of rectangle and returns it.
     * @return lowerLeft Point in the lower left corner of rectangle.
     */
    private Point getLowerLeft() {
        return lowerLeft;
    }
    /**
     * Gets upperRight Point of rectangle and returns it.
     * @return upperRight Point in the upper right corner of rectangle.
     */
    private Point getUpperRight() {
        return upperRight;
    }
    /**
     * Gets lowerRight Point of rectangle and returns it.
     * @return lowerRight Point in the lower right corner of rectangle.
     */
    private Point getLowerRight() {
        return lowerRight;
    }
    /**
     * Gets upperLeft Point of rectangle and returns it.
     * @return upperLeft Point in the upper left corner of rectangle.
     */
    private Point getUpperLeft() {
        return upperLeft;
    }

    /**
     * Returns the smallest Rectangle that contains all points in this Rectangle and in other.
     * Returns null if other == null.
     * @param other an instance of Rectangle;
     * @return Rectangle smallest Rectangle containing this Rectangle and other.
     */
    public Rectangle boundingBox(Rectangle other) {
        if(other == null){
            return null;
        }

        int smallX;
        int smallY;
        int bigX;
        int bigY;

        if(lowerLeft.getX() < other.getLowerLeft().getX()) {
            smallX = lowerLeft.getX();
        }
        else {
            smallX = other.getLowerLeft().getX();
        }
        if(upperRight.getX() > other.getUpperRight().getX()) {
            bigX = upperRight.getX();
        }
        else {
            bigX = other.getUpperRight().getX();
        }
        if(lowerLeft.getY() < other.getLowerLeft().getY()) {
            smallY = lowerLeft.getY();
        }
        else {
            smallY = other.getLowerLeft().getY();
        }
        if(upperRight.getY() > other.getUpperRight().getY()) {
            bigY = upperRight.getY();
        }
        else {
            bigY = other.getUpperRight().getY();
        }

        Point point1 = new Point(smallX, smallY);
        Point point2 = new Point(bigX, bigY);

        return (new Rectangle(point1, point2));

    }

    /**
     * Returns whether this Rectangle instance contains the Point point by comparing Rectangle's coordinates with
     * Point coordinates. If any of clauses is false it return false.
     * @param point any given Point;
     * @return boolean
     */
    public boolean  contains(Point point) {
        return point != null && (point.getX() >= lowerLeft.getX()) && (point.getX() <= lowerRight.getX()) &&
                (point.getY() >= lowerLeft.getY()) && (point.getY() <= upperRight.getY());
    }


    /**
     * Returns whether this Rectangle instance contains all points that the other contains.
     * If rectangle contains points which lie in opposite corners of other rectangle, it contains the whole rectangle.
     * @param other any given Rectangle;
     * @return boolean return true if this Rectangle contains two opposite corners of other.
     */
    public boolean contains(Rectangle other) {
        if(other == null) {
            return false;
        }
        return (Rectangle.this.contains(other.getLowerLeft()) && Rectangle.this.contains(other.getUpperRight()));
    }

    /**
     * A method checks if two given lines intersects assuming that they are on the same axis, and returns start and end
     * points of mutual points if such points exists, otherwise it returns null.
     * Line1x1, line1x2 is start and end point of one line and line2x1, line2x2 of the other.
     * @param line1x1 smallest point of line one;
     * @param line1x2 biggest point of line one;
     * @param line2x1 smallest point of line two;
     * @param line2x2 biggest point of line two;
     * @return int[] an array of start and end point of intersection line.
     */
    private int[] lineIntersection(int line1x1, int line1x2, int line2x1, int line2x2) {
        int[] ints = new int[2];
        if(line1x1 <= line2x1 && line1x2 >= line2x1) {
            ints[0] = line2x1;
            if(line1x2 >= line2x2) {
                ints[1] = line2x2;
            }
            else {
                ints[1] = line1x2;
            }
        }
        else if(line1x1 > line2x1 && line1x1 <= line2x2){
            ints[0] = line1x1;
            if(line1x2 >= line2x2) {
                ints[1] = line2x2;
            }
            else {
                ints[1] = line1x2;
            }
        }
        else {
            return null;
        }
        return ints;
    }

    /**
     * Returns a Rectangle instance that contains exactly the points that both this Rectangle instance
     * and other contain, provided that such points exist. Returns null otherwise.
     * @param other any given Rectangle;
     * @return Rectangle instance that contains exactly the points that both this Rectangle instance and other contain.
     */
    public Rectangle intersection(Rectangle other) {
        if(other == null) {
            return null;
        }

        Point thisLowerLeft = Rectangle.this.getLowerLeft();
        Point thisUpperRight = Rectangle.this.getUpperRight();

        int[] xAxis = lineIntersection(thisLowerLeft.getX(), thisUpperRight.getX(), other.getLowerLeft().getX(), other.getUpperRight().getX());
        int[] yAxis = lineIntersection(thisLowerLeft.getY(), thisUpperRight.getY(), other.getLowerLeft().getY(), other.getUpperRight().getY());

        if(xAxis != null && yAxis != null) {
            Point point1 = new Point(xAxis[0], yAxis[0]);
            Point point2 = new Point(xAxis[1], yAxis[1]);
            return (new Rectangle(point1, point2));
        }
        return null;
    }

    /**
     * Returns whether this Rectangle instance and other intersects. If there is at least one point that both rectangle
     * contain it returns true.
     * @param other Rectangle instance.
     * @return boolean true if at least one point that both rectangle contain is the same.
     */
    public boolean intersects(Rectangle other) {
        return intersection(other) != null;
    }

    /**
     * Returns the number of points this Rectangle contains. If Rectangle is of a single point ir returns 1.
     * @return int number of point this Rectangle contains.
     */
    public int numberOfPoints() {
        return ((1 + abs(lowerLeft.getX() - lowerRight.getX())) * (1 + abs(lowerLeft.getY() - upperLeft.getY())));
    }

    /**
     * Returns a string representation of the object Rectangle in the format:
     * "Rectangle with corners: [Point, Point, Point, Point], where Point is a string representation of Point.
     * @return a string representation of Rectangle.
     */
    @Override public String toString() {
        return ("Rectangle with corners: [" + lowerLeft + ", " + upperLeft + ", " + lowerRight + ", " + upperRight + "]");
    }

    /**
     * Returns whether all Rectangle in rectangles intersect with each other. If rectangles contains 0 elements,
     * it returns true.
     * @param rectangles an array of Rectangle with possible null entries;
     * @return boolean returns false if any Rectangle doesn't intersect with any other rectangle.
     */
    public static boolean allIntersect(Rectangle[] rectangles) {
        if(rectangles.length > 0){
            for(int i = 0; i < rectangles.length; i++) {
                for(int j = i; j < rectangles.length; j++) {
                    if (!rectangles[i].intersects(rectangles[j])) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

}
