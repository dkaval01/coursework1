import java.util.Arrays;

/**
 * Test class for Rectangle and Point classes.
 *
 * @author Carsten Fuhs
 */
public class RectangleMain {

    /**
     * Runs some tests for Rectangle and Point.
     *
     * @param args Ignored.
     */
    public static void main(String[] args) {
        Point[] allPoints = {
                new Point(  -2,   5), // 0
                new Point(   4,   1), // 1
                new Point(  -2,   1), // 2
                new Point(   4,   5), // 3
                new Point(  -3,  -1), // 4
                new Point(   4,   3), // 5
                new Point(-100, 200), // 6
                new Point( 300,-400)  // 7
        };
        Point[] someNullPoints = {
                allPoints[0], allPoints[2], null, allPoints[4], null
        };

        Rectangle[] allRectangles = {
                new Rectangle(allPoints[0], allPoints[1]), // 0
                new Rectangle(allPoints[2], allPoints[3]), // 1
                new Rectangle(allPoints[3], allPoints[2]), // 2
                new Rectangle(allPoints[4], allPoints[5]), // 3
                new Rectangle(allPoints[6], allPoints[7]), // 4
                new Rectangle(allPoints[5], allPoints[5]), // 5
                new Rectangle(allPoints[5], allPoints[3]), // 6
                new Rectangle(allPoints[3], allPoints[1]), // 7
                new Rectangle(allPoints[6], allPoints[4]), // 8
                new Rectangle(allPoints[7], allPoints[5]), // 9
        };

        Rectangle[] firstRectangles = Arrays.copyOfRange(allRectangles, 0, 5);
        Rectangle[] someNullRectangles = Arrays.copyOfRange(allRectangles, 0, 20);
        Rectangle[] oneRectangle = { allRectangles[9] };

        System.out.println("(01) Points.boundingBox:");
        System.out.println("ACTUAL:   " + Point.boundingBox(allPoints));
        System.out.println("EXPECTED: Rectangle with corners: [(-100,-400), (-100,200), (300,-400), (300,200)]");

        System.out.println("(02) Points.boundingBox:");
        System.out.println("ACTUAL:   " + Point.boundingBox(someNullPoints));
        System.out.println("EXPECTED: null");

        System.out.println("(03) Rectangle.boundingBox:");
        System.out.println("ACTUAL:   " + allRectangles[0].boundingBox(allRectangles[1]));
        System.out.println("EXPECTED: Rectangle with corners: [(-2,1), (-2,5), (4,1), (4,5)]");

        System.out.println("(04) Rectangle.boundingBox:");
        System.out.println("ACTUAL:   " + allRectangles[1].boundingBox(allRectangles[0]));
        System.out.println("EXPECTED: Rectangle with corners: [(-2,1), (-2,5), (4,1), (4,5)]");

        System.out.println("(05) Rectangle.boundingBox:");
        System.out.println("ACTUAL:   " + allRectangles[5].boundingBox(allRectangles[5]));
        System.out.println("EXPECTED: Rectangle with corners: [(4,3), (4,3), (4,3), (4,3)]");

        System.out.println("(06) Rectangle.boundingBox:");
        System.out.println("ACTUAL:   " + allRectangles[8].boundingBox(allRectangles[9]));
        System.out.println("EXPECTED: Rectangle with corners: [(-100,-400), (-100,200), (300,-400), (300,200)]");

        System.out.println("(07) Rectangle.boundingBox:");
        System.out.println("ACTUAL:   " + allRectangles[6].boundingBox(allRectangles[9]));
        System.out.println("EXPECTED: Rectangle with corners: [(4,-400), (4,5), (300,-400), (300,5)]");

        System.out.println("(08) Rectangle.contains(Point):");
        System.out.println("ACTUAL:   " + allRectangles[8].contains((Point) null));
        System.out.println("EXPECTED: false");

        System.out.println("(09) Rectangle.contains(Point):");
        System.out.println("ACTUAL:   " + allRectangles[8].contains(allPoints[4]));
        System.out.println("EXPECTED: true");

        System.out.println("(10) Rectangle.contains(Point):");
        System.out.println("ACTUAL:   " + allRectangles[4].contains(allPoints[3]));
        System.out.println("EXPECTED: true");

        System.out.println("(11) Rectangle.contains(Point):");
        System.out.println("ACTUAL:   " + allRectangles[0].contains(allPoints[7]));
        System.out.println("EXPECTED: false");

        System.out.println("(12) Rectangle.contains(Rectangle):");
        System.out.println("ACTUAL:   " + allRectangles[0].contains((Rectangle) null));
        System.out.println("EXPECTED: false");

        System.out.println("(13) Rectangle.contains(Rectangle):");
        System.out.println("ACTUAL:   " + allRectangles[0].contains(allRectangles[0]));
        System.out.println("EXPECTED: true");

        System.out.println("(14) Rectangle.contains(Rectangle):");
        System.out.println("ACTUAL:   " + allRectangles[0].contains(allRectangles[1]));
        System.out.println("EXPECTED: true");

        System.out.println("(15) Rectangle.contains(Rectangle):");
        System.out.println("ACTUAL:   " + allRectangles[1].contains(allRectangles[0]));
        System.out.println("EXPECTED: true");

        System.out.println("(16) Rectangle.contains(Rectangle):");
        System.out.println("ACTUAL:   " + allRectangles[4].contains(allRectangles[0]));
        System.out.println("EXPECTED: true");

        System.out.println("(17) Rectangle.contains(Rectangle):");
        System.out.println("ACTUAL:   " + allRectangles[0].contains(allRectangles[4]));
        System.out.println("EXPECTED: false");

        System.out.println("(18) Rectangle.intersection:");
        System.out.println("ACTUAL:   " + allRectangles[0].intersection(null));
        System.out.println("EXPECTED: null");

        System.out.println("(19) Rectangle.intersection:");
        System.out.println("ACTUAL:   " + allRectangles[0].intersection(allRectangles[4]));
        System.out.println("EXPECTED: Rectangle with corners: [(-2,1), (-2,5), (4,1), (4,5)]");

        System.out.println("(20) Rectangle.intersection:");
        System.out.println("ACTUAL:   " + allRectangles[1].intersection(allRectangles[0]));
        System.out.println("EXPECTED: Rectangle with corners: [(-2,1), (-2,5), (4,1), (4,5)]");

        System.out.println("(21) Rectangle.intersection:");
        System.out.println("ACTUAL:   " + allRectangles[3].intersection(allRectangles[0]));
        System.out.println("EXPECTED: Rectangle with corners: [(-2,1), (-2,3), (4,1), (4,3)]");

        System.out.println("(22) Rectangle.intersection:");
        System.out.println("ACTUAL:   " + allRectangles[8].intersection(allRectangles[5]));
        System.out.println("EXPECTED: null");

        System.out.println("(23) Rectangle.intersects:");
        System.out.println("ACTUAL:   " + allRectangles[8].intersects(null));
        System.out.println("EXPECTED: false");

        System.out.println("(24) Rectangle.intersects:");
        System.out.println("ACTUAL:   " + allRectangles[1].intersects(allRectangles[0]));
        System.out.println("EXPECTED: true");

        System.out.println("(25) Rectangle.intersects:");
        System.out.println("ACTUAL:   " + allRectangles[5].intersects(allRectangles[8]));
        System.out.println("EXPECTED: false");

        System.out.println("(26) Rectangle.numberOfPoints:");
        System.out.println("ACTUAL:   " + allRectangles[0].numberOfPoints());
        System.out.println("EXPECTED: 35");

        System.out.println("(27) Rectangle.numberOfPoints:");
        System.out.println("ACTUAL:   " + allRectangles[5].numberOfPoints());
        System.out.println("EXPECTED: 1");

        System.out.println("(28) Rectangle.numberOfPoints:");
        System.out.println("ACTUAL:   " + allRectangles[8].numberOfPoints());
        System.out.println("EXPECTED: 19796");

        System.out.println("(29) Rectangle.numberOfPoints:");
        System.out.println("ACTUAL:   " + allRectangles[6].numberOfPoints());
        System.out.println("EXPECTED: 3");

        System.out.println("(30) Rectangle.allIntersect:");
        System.out.println("ACTUAL:   " + Rectangle.allIntersect(someNullRectangles));
        System.out.println("EXPECTED: false");

        System.out.println("(31) Rectangle.allIntersect:");
        System.out.println("ACTUAL:   " + Rectangle.allIntersect(allRectangles));
        System.out.println("EXPECTED: false");

        System.out.println("(32) Rectangle.allIntersect:");
        System.out.println("ACTUAL:   " + Rectangle.allIntersect(firstRectangles));
        System.out.println("EXPECTED: true");

        System.out.println("(33) Rectangle.allIntersect:");
        System.out.println("ACTUAL:   " + Rectangle.allIntersect(new Rectangle[0]));
        System.out.println("EXPECTED: true");

        System.out.println("(34) Rectangle.allIntersect:");
        System.out.println("ACTUAL:   " + Rectangle.allIntersect(oneRectangle));
        System.out.println("EXPECTED: true");
    }
}
